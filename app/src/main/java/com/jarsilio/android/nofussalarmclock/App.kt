package com.jarsilio.android.nofussalarmclock

import android.app.Application
import com.jarsilio.android.common.logging.LongTagTree
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        Timber.plant(LongTagTree(this))
    }
}
