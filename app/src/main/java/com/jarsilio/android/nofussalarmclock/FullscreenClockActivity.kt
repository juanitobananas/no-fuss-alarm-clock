package com.jarsilio.android.nofussalarmclock

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import android.widget.TextView
import java.util.Calendar
import com.jarsilio.android.nofussalarmclock.extensions.utils
import timber.log.Timber

open class FullscreenClockActivity : AppCompatActivity() {

    protected val handler: Handler by lazy { Handler() }
    protected val clockTextView: TextView by lazy { findViewById<TextView>(R.id.clock_text_view) }
    protected val belowClockTextView: TextView by lazy { findViewById<TextView>(R.id.below_clock_text_view_1) }

    private fun showOverLockScreen() {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD or
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        showOverLockScreen()
        setContentView(R.layout.activity_fullscreen_clock)

        clockTextView.text = utils.getTimeString(Calendar.getInstance())
    }

    override fun onStop() {
        Timber.d("Removing Handler() callbacks")
        handler.removeCallbacksAndMessages(null)

        super.onStop()
    }
}
