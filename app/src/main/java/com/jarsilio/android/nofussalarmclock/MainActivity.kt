package com.jarsilio.android.nofussalarmclock

import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import androidx.fragment.app.Fragment
import androidx.appcompat.widget.SwitchCompat
import android.text.format.DateFormat
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import android.widget.TimePicker
import com.jarsilio.android.common.menu.CommonMenu
import com.jarsilio.android.nofussalarmclock.extensions.alarmHandler
import com.jarsilio.android.nofussalarmclock.extensions.prefs
import com.jarsilio.android.nofussalarmclock.extensions.utils
import timber.log.Timber
import java.util.Calendar

const val BATTERY_OPTIMIZATION_REQUEST_CODE = 100

class MainActivity : AppCompatActivity(), TimePickerFragment.TimerPickerCallback {

    private val commonMenu: CommonMenu by lazy { CommonMenu(this) }

    private val alarmTextView: TextView by lazy { findViewById<TextView>(R.id.alarm_text_view) }
    private val enableSwitch: SwitchCompat by lazy { findViewById<SwitchCompat>(R.id.enable_switch) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        alarmTextView.setOnClickListener { TimePickerFragment().show(supportFragmentManager, "timePicker") }
        enableSwitch.apply {
            setOnCheckedChangeListener { _, isChecked ->
                if (prefs.isEnabled != isChecked) { // Only re-set or cancel alarms if value changed of enabled actually changed
                    if (isChecked) {
                        Timber.d("Enabled. (Re-)Setting alarms...")
                        alarmHandler.setAlarm(prefs.alarmHourOfDay, prefs.alarmMinute)
                    } else {
                        Timber.d("Disabled. Canceling alarms...")
                        alarmHandler.cancelAlarms()
                    }
                    prefs.isEnabled = isChecked
                }
            }
        }

        requestIgnoreBatteryOptimization()
    }

    private fun requestIgnoreBatteryOptimization() {
        Timber.d("Requesting to ignore battery optimizations for WaveUp")
        startActivityForResult(Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS, Uri.parse("package:$packageName")), BATTERY_OPTIMIZATION_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == BATTERY_OPTIMIZATION_REQUEST_CODE) {
            // TODO check if the user allowed it or not and show some message
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        // This is also called when the notification drawer is closed (which happens if you dismiss from the notification)
        super.onWindowFocusChanged(hasFocus)

        alarmTextView.text = utils.getTimeString(prefs.alarmHourOfDay, prefs.alarmMinute)
        enableSwitch.isChecked = prefs.isEnabled
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        commonMenu.addImpressumToMenu(menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menu_item_settings -> startActivity(Intent(this, PreferencesActivity::class.java))
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onAttachFragment(fragment: Fragment) {
        super.onAttachFragment(fragment)

        if (fragment is TimePickerFragment) {
            val timePickerFragment = fragment as TimePickerFragment?
            timePickerFragment?.setTimerPickerCallback(this)
        }
    }

    override fun onTimeUpdated(hourOfDay: Int, minute: Int) {
        Timber.d("onTimeUpdated callback: $hourOfDay:$minute")

        val alarmTextView = findViewById<TextView>(R.id.alarm_text_view)
        alarmTextView.text = utils.getTimeString(hourOfDay, minute)

        prefs.alarmHourOfDay = hourOfDay
        prefs.alarmMinute = minute

        alarmHandler.setAlarm(hourOfDay, minute)
    }
}

class TimePickerFragment : androidx.fragment.app.DialogFragment(), TimePickerDialog.OnTimeSetListener {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val hourOfDay: Int
        val minute: Int

        if (context != null) {
            hourOfDay = context!!.prefs.alarmHourOfDay
            minute = context!!.prefs.alarmMinute
        } else {
            val calendar = Calendar.getInstance()
            hourOfDay = calendar.get(Calendar.HOUR_OF_DAY)
            minute = calendar.get(Calendar.MINUTE) + 10
        }

        return TimePickerDialog(activity, this, hourOfDay, minute, DateFormat.is24HourFormat(activity))
    }

    override fun onTimeSet(view: TimePicker, hourOfDay: Int, minute: Int) {
        // Do something with the time chosen by the user
        Timber.d("Selected time: $hourOfDay:$minute")
        callback?.onTimeUpdated(hourOfDay, minute)
    }

    private var callback: TimerPickerCallback? = null

    interface TimerPickerCallback {
        fun onTimeUpdated(hourOfDay: Int, minute: Int)
    }

    fun setTimerPickerCallback(callback: TimerPickerCallback) {
        this.callback = callback
    }
}