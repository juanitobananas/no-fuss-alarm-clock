package com.jarsilio.android.nofussalarmclock

import android.content.SharedPreferences
import android.os.Bundle
import android.preference.ListPreference
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import timber.log.Timber
import android.media.RingtoneManager
import android.content.Intent
import android.net.Uri
import android.provider.Settings.System.DEFAULT_ALARM_ALERT_URI
import com.jarsilio.android.nofussalarmclock.extensions.prefs

const val REQUEST_CODE_ALERT_RINGTONE = 11

class PreferencesActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Display the fragment as the main content.
        supportFragmentManager.beginTransaction().replace(android.R.id.content, PrefsFragment()).commit()

        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        Timber.d("Changed preference: $key")
    }

    class PrefsFragment : PreferenceFragmentCompat() {

        override fun onCreatePreferences(p0: Bundle?, p1: String?) {
            addPreferencesFromResource(R.xml.preferences)
        }

        override fun onPreferenceTreeClick(preference: Preference?): Boolean {
            return if (preference?.key == getString(R.string.pref_ringtone_key)) {
                val intent = Intent(RingtoneManager.ACTION_RINGTONE_PICKER).apply {
                    putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_ALARM)
                    putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true)
                    putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false)
                    putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, DEFAULT_ALARM_ALERT_URI)
                    putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, context?.prefs?.ringtone)
                }

                startActivityForResult(intent, REQUEST_CODE_ALERT_RINGTONE)

                true
            } else {
                super.onPreferenceTreeClick(preference)
            }
        }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            if (requestCode == REQUEST_CODE_ALERT_RINGTONE && data != null) {
                val ringtone = data.getParcelableExtra<Uri>(RingtoneManager.EXTRA_RINGTONE_PICKED_URI)
                context?.prefs?.ringtone = ringtone
            } else {
                super.onActivityResult(requestCode, resultCode, data)
            }
        }

        companion object {

            /**
             * A preference value change listener that updates the preference's summary
             * to reflect its new value.
             */
            private val sBindPreferenceSummaryToValueListener = Preference.OnPreferenceChangeListener { preference, value ->
                val stringValue = value.toString()

                if (preference is ListPreference) {
                    // For list preferences, look up the correct display value in
                    // the preference's 'entries' list.
                    val index = preference.findIndexOfValue(stringValue)

                    // Set the summary to reflect the new value.
                    preference.setSummary(
                        if (index >= 0)
                            preference.entries[index]
                        else
                            null)
                } else {
                    // For all other preferences, set the summary to the value's
                    // simple string representation.
                    preference.summary = stringValue
                }
                true
            }

            /**
             * Binds a preference's summary to its value. More specifically, when the
             * preference's value is changed, its summary (line of text below the
             * preference title) is updated to reflect the value. The summary is also
             * immediately updated upon calling this method. The exact display format is
             * dependent on the type of preference.

             * @see .sBindPreferenceSummaryToValueListener
             */
            private fun bindPreferenceSummaryToValue(preference: Preference) {
                // Set the listener to watch for value changes.
                preference.onPreferenceChangeListener =
                    sBindPreferenceSummaryToValueListener

                // Trigger the listener immediately with the preference's
                // current value.
                sBindPreferenceSummaryToValueListener.onPreferenceChange(preference,
                    PreferenceManager
                        .getDefaultSharedPreferences(preference.context)
                        .getString(preference.key, ""))
            }
        }
    }
}
