package com.jarsilio.android.nofussalarmclock

import android.os.Bundle
import android.view.MotionEvent
import com.jarsilio.android.nofussalarmclock.extensions.prefs
import com.jarsilio.android.nofussalarmclock.extensions.utils
import timber.log.Timber
import java.util.Calendar

const val SHOWTIME_SECONDS: Long = 5

class SnoozedUntilActivity : FullscreenClockActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val snoozedUntil = utils.getTimeString(Calendar.getInstance().apply { add(Calendar.MINUTE, prefs.snoozeTime) })

        belowClockTextView.text = getString(R.string.snoozed_until, snoozedUntil)

        handler.postDelayed({
            Timber.d("Showed 'snoozed until' Activity for $SHOWTIME_SECONDS seconds. Finishing activity...")
            finish()
        }, SHOWTIME_SECONDS * 1000L)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event?.action == MotionEvent.ACTION_UP) {
            Timber.d("User tapped on 'Snoozed until' screen. Finish()ing Activity...")
            finish()
        }

        return super.onTouchEvent(event)
    }

    override fun onPause() {
        super.onPause()

        finish()
    }
}
