package com.jarsilio.android.nofussalarmclock

import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.TypedValue
import android.view.View
import com.jarsilio.android.nofussalarmclock.extensions.prefs
import com.jarsilio.android.nofussalarmclock.extensions.utils
import android.view.animation.Animation
import android.view.animation.AlphaAnimation
import android.widget.Button
import android.widget.RelativeLayout
import android.widget.TextView
import com.jarsilio.android.nofussalarmclock.extensions.alarmHandler
import com.jarsilio.android.nofussalarmclock.extensions.notificationHandler
import com.jarsilio.android.nofussalarmclock.extensions.ringtoneHandler
import com.jarsilio.android.nofussalarmclock.extensions.vibrationHandler
import timber.log.Timber
import java.util.*
import kotlin.math.roundToInt

const val MIN_NUMBER_OF_TAPS_TO_SNOOZE = 2
const val BUTTON_DIAMETER_IN_DP = 100

class WakieWakieActivity : FullscreenClockActivity() {

    private val button: Button by lazy { findViewById<Button>(R.id.jumpy_dismiss_button) }
    private val snoozeText: TextView by lazy { findViewById<TextView>(R.id.below_clock_text_view_1) }
    private val fadeIn = AlphaAnimation(0.0f, 1.0f).apply { duration = 600 }
    private val fadeOut = AlphaAnimation(1.0f, 0.0f).apply { duration = 600 }

    private val tapsToSnooze: Int
        get() {
            return if (prefs.increaseTapsPerSnooze) {
                val tapsToSnooze = MIN_NUMBER_OF_TAPS_TO_SNOOZE + utils.getTimeSinceAlarm() / prefs.snoozeTime
                Timber.d("Number of times to tap to snooze: $tapsToSnooze")
                tapsToSnooze
            } else {
                MIN_NUMBER_OF_TAPS_TO_SNOOZE
            }
        }

    private var tapCount = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        handler.postDelayed({
            snooze()
        }, this.prefs.maxRingTime * 60 * 1000L)

        updateSnoozeText()
        initButton()
        fireAlarm()

        // See: https://github.com/LineageOS/android_packages_apps_DeskClock/blob/lineage-16.0/src/com/android/deskclock/alarms/AlarmActivity.java#L187
        sendBroadcast(Intent(Intent.ACTION_CLOSE_SYSTEM_DIALOGS))
    }

    private fun initButton() {
        // Hack: force button width and height (if not, I would get -2 and -2 while reading them, although they were set with the shape in an xml)
        button.layoutParams.width = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, BUTTON_DIAMETER_IN_DP.toFloat(), resources.displayMetrics).roundToInt()
        button.layoutParams.height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, BUTTON_DIAMETER_IN_DP.toFloat(), resources.displayMetrics).roundToInt()

        randomizeButtonPosition()
        button.visibility = View.VISIBLE
        button.setOnClickListener {
            tapCount++

            if (tapCount == tapsToSnooze) {
                snooze()
            }
            updateSnoozeText()
            moveButton()
        }
    }

    private fun updateSnoozeText() {
        val tapsLeft = tapsToSnooze - tapCount
        snoozeText.text = when (tapsLeft) {
            0 -> getString(R.string.snoozed)
            1 -> getString(R.string.tap_1_time_to_snooze)
            else -> getString(R.string.tap_n_times_to_snooze, tapsLeft.toString())
        }
    }

    private fun moveButton() {
        button.startAnimation(fadeOut)
        fadeOut.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation) {}

            override fun onAnimationEnd(animation: Animation) {
                randomizeButtonPosition()
                button.startAnimation(fadeIn)
            }

            override fun onAnimationRepeat(animation: Animation) {}
        })
    }

    private fun randomizeButtonPosition() {
        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        val width = displayMetrics.widthPixels
        val height = displayMetrics.heightPixels

        val layoutParams = button.layoutParams as RelativeLayout.LayoutParams

        layoutParams.leftMargin = Random().nextInt(width - layoutParams.width)
        layoutParams.topMargin = Random().nextInt(height - layoutParams.height)
        button.layoutParams = layoutParams
    }

    private fun fireAlarm() {
        blinkClock()
        ringtoneHandler.playRingtone()
        vibrationHandler.startVibrationIfNecessary()
        notificationHandler.showWakieWakieNotification()
    }

    private fun blinkClock() {
        val blinkAnimation = AlphaAnimation(0.0f, 1.0f).apply {
            duration = 700
            startOffset = 200
            repeatMode = Animation.REVERSE
            repeatCount = Animation.INFINITE
        }

        clockTextView.startAnimation(blinkAnimation)
    }

    private fun snooze() {
        ringtoneHandler.stopRingtone()
        vibrationHandler.stopVibrationIfNecessary()
        alarmHandler.snooze()
        startActivity(Intent(this, SnoozedUntilActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
        })
        finish()
    }

    override fun onBackPressed() {
        Timber.d("Ignoring back button...")
    }
}
