package com.jarsilio.android.nofussalarmclock.extensions

import android.annotation.TargetApi
import android.app.AlarmManager
import android.app.PendingIntent
import android.os.Build
import java.util.Calendar

fun AlarmManager.setExact(calendar: Calendar, pendingIntent: PendingIntent) {
    return setExact(calendar.timeInMillis, pendingIntent)
}

@TargetApi(Build.VERSION_CODES.KITKAT)
fun AlarmManager.setExact(timeInMillis: Long, pendingIntent: PendingIntent) {
    when {
        Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT -> set(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent)
        Build.VERSION.SDK_INT >= Build.VERSION_CODES.M -> setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent)
        else -> setExact(AlarmManager.RTC_WAKEUP, timeInMillis, pendingIntent) // between KITKAT and M
    }
}