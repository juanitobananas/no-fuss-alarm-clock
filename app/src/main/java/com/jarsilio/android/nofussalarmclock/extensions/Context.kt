package com.jarsilio.android.nofussalarmclock.extensions

import android.content.Context
import com.jarsilio.android.nofussalarmclock.prefs.Prefs
import com.jarsilio.android.nofussalarmclock.utils.AlarmHandler
import com.jarsilio.android.nofussalarmclock.utils.NotificationHandler
import com.jarsilio.android.nofussalarmclock.utils.RingtoneHandler
import com.jarsilio.android.nofussalarmclock.utils.VibrationHandler
import com.jarsilio.android.nofussalarmclock.utils.Utils

val Context.prefs: Prefs
    get() = Prefs.getInstance(this)

val Context.utils: Utils
    get() = Utils.getInstance(this)

val Context.ringtoneHandler: RingtoneHandler
    get() = RingtoneHandler.getInstance(this)

val Context.vibrationHandler: VibrationHandler
    get() = VibrationHandler.getInstance(this)

val Context.alarmHandler: AlarmHandler
    get() = AlarmHandler.getInstance(this)

val Context.notificationHandler: NotificationHandler
    get() = NotificationHandler.getInstance(this)