package com.jarsilio.android.nofussalarmclock.prefs

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.preference.PreferenceManager
import android.provider.Settings
import com.jarsilio.android.nofussalarmclock.R
import com.jarsilio.android.nofussalarmclock.models.SingletonHolder

class Prefs private constructor(context: Context) {
    private val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    /* User prefs */
    private val IS_ENABLED: String = context.getString(R.string.pref_enabled_key)
    private val SNOOZE_TIME: String = context.getString(R.string.pref_snooze_time_key)
    private val MAX_RING_TIME: String = context.getString(R.string.pref_max_ring_time_key)
    private val VIBRATE: String = context.getString(R.string.pref_vibrate_key)
    private val RINGTONE = context.getString(R.string.pref_ringtone_key)
    private val INCREASE_TAPS_PER_SNOOZE: String = context.getString(R.string.pref_increase_tabs_per_snooze)

    var isEnabled: Boolean
        get() = prefs.getBoolean(IS_ENABLED, false)
        set(value) = setBoolean(IS_ENABLED, value)

    var ringtone: Uri
        get() {
            val ringtone = prefs.getString(RINGTONE, "no ringtone set yet")
            return if (ringtone == "no ringtone set yet") {
                Settings.System.DEFAULT_ALARM_ALERT_URI
            } else {
                Uri.parse(ringtone)
            }
        }
        set(value) = setUri(RINGTONE, value)

    var snoozeTime: Int
        get() = prefs.getString(SNOOZE_TIME, "7").toInt()
        set(value) = setString(SNOOZE_TIME, value)

    var maxRingTime: Int
        get() = prefs.getInt(MAX_RING_TIME, 10)
        set(value) = setInt(MAX_RING_TIME, value)

    var vibrate: Boolean
        get() = prefs.getBoolean(VIBRATE, false)
        set(value) = setBoolean(VIBRATE, value)

    var increaseTapsPerSnooze: Boolean
        get() = prefs.getBoolean(INCREASE_TAPS_PER_SNOOZE, true)
        set(value) = setBoolean(INCREASE_TAPS_PER_SNOOZE, value)

    /* Automagic prefs: these are not real prefs. They are just set by the app to remember stuff */
    private val ALARM_HOUR_OF_DAY: String = context.getString(R.string.pref_alarm_hour_of_day_key)
    private val ALARM_MINUTE: String = context.getString(R.string.pref_alarm_minute_key)

    var alarmHourOfDay: Int
        get() = prefs.getInt(ALARM_HOUR_OF_DAY, 7)
        set(value) = setInt(ALARM_HOUR_OF_DAY, value)

    var alarmMinute: Int
        get() = prefs.getInt(ALARM_MINUTE, 42)
        set(value) = setInt(ALARM_MINUTE, value)

    private fun setBoolean(key: String, value: Boolean) { prefs.edit().putBoolean(key, value).apply() }
    private fun setInt(key: String, value: Int) { prefs.edit().putInt(key, value).apply() }
    private fun setString(key: String, value: Int) { prefs.edit().putString(key, value.toString()).apply() }
    private fun setUri(key: String, value: Uri?) { prefs.edit().putString(key, value?.toString()).apply() }

    companion object : SingletonHolder<Prefs, Context>(::Prefs)
}