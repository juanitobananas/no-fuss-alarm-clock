package com.jarsilio.android.nofussalarmclock.utils

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.core.app.JobIntentService
import com.jarsilio.android.nofussalarmclock.WakieWakieActivity
import com.jarsilio.android.nofussalarmclock.extensions.*
import com.jarsilio.android.nofussalarmclock.models.SingletonHolder
import timber.log.Timber
import java.util.Calendar

const val EXTRA_SCHEDULER_TYPE: String = "EXTRA_SCHEDULER_TYPE"
enum class SchedulerType(val requestCode: Int) {
    ALARM(100), NOTIFICATION(200)
}

class AlarmHandler private constructor(private val context: Context) {

    private val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager?

    private val alarmPendingIntent: PendingIntent
        get() {
            val intent = Intent(context, AlarmReceiver::class.java).apply {
                putExtra(EXTRA_SCHEDULER_TYPE, SchedulerType.ALARM.ordinal)
            }
            return PendingIntent.getBroadcast(context, SchedulerType.ALARM.requestCode, intent, PendingIntent.FLAG_CANCEL_CURRENT)
        }

    private val notificationPendingIntent: PendingIntent
        get() {
            val intent = Intent(context, AlarmReceiver::class.java).apply {
                putExtra(EXTRA_SCHEDULER_TYPE, SchedulerType.NOTIFICATION.ordinal)
            }
            return PendingIntent.getBroadcast(context, SchedulerType.NOTIFICATION.requestCode, intent, PendingIntent.FLAG_CANCEL_CURRENT)
        }

    fun setAlarm(hourOfDay: Int, minute: Int, isSnooze: Boolean = false) {
        cancelAlarms()

        val alarmTime = context.utils.getAlarmCalendar(hourOfDay, minute)
        val twoHoursBefore = context.utils.getTwoHoursBeforeCalendar(alarmTime)

        Timber.d("Setting alarm for ${context.utils.getTimeString(alarmTime)}")
        alarmManager?.setExact(alarmTime, alarmPendingIntent)
        if (isSnooze) {
            Timber.d("Setting snooze notification now for ${context.utils.getTimeString(alarmTime)}")
            context.notificationHandler.showSnoozeNotification()
        } else {
            Timber.d("Setting notification for ${context.utils.getTimeString(twoHoursBefore)} (one hourOfDay before)")
            alarmManager?.setExact(twoHoursBefore, notificationPendingIntent)
        }

        context.prefs.isEnabled = true
    }

    fun cancelAlarms() {
        Timber.d("Canceling previously set 'scheduled jobs' (alarm and notification) (if any)")
        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
        alarmManager?.apply {
            cancel(alarmPendingIntent)
            cancel(notificationPendingIntent)
        }

        Timber.d("Hiding notifications (if any)")
        context.notificationHandler.hideNotifications()
        Timber.d("Stopping ringtone (if ringing)")
        context.ringtoneHandler.stopRingtone()

        context.prefs.isEnabled = false
    }

    fun snooze() {
        val calendar = Calendar.getInstance()
        calendar.add(Calendar.MINUTE, context.prefs.snoozeTime)

        val newHourOfDay = calendar.get(Calendar.HOUR_OF_DAY)
        val newMinute = calendar.get(Calendar.MINUTE)

        Timber.d("Snoozed until: ${context.utils.getTimeString(newHourOfDay, newMinute)}")

        setAlarm(newHourOfDay, newMinute, true)
    }

    companion object : SingletonHolder<AlarmHandler, Context>(::AlarmHandler)
}

class AlarmReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        when (intent.getIntExtra(EXTRA_SCHEDULER_TYPE, -42)) {
            SchedulerType.ALARM.ordinal -> run {
                Timber.d("Android alarm (scheduled job) received: Time for an actual alarm (with its ringtone and everything) has arrived")
            }
            SchedulerType.NOTIFICATION.ordinal -> run {
                Timber.d("Android alarm (scheduled job) received: Time to show an 'upcoming alarm' notification")
            }
            else -> Timber.e("Error while receiving alarm (no EXTRA_SCHEDULER_TYPE)")
        }

        Timber.d("Launching an AlarmJobIntentService that will take necessary action")
        AlarmJobIntentService.enqueueWork(context, intent)
    }
}

const val JOB_ID = 1000

class AlarmJobIntentService : JobIntentService() {

    override fun onHandleWork(intent: Intent) {

        when (intent.getIntExtra(EXTRA_SCHEDULER_TYPE, -42)) {
            SchedulerType.ALARM.ordinal -> run {
                Timber.d("Firing alarm: Starting WakieWakieActivity")
                startActivity(Intent(this, WakieWakieActivity::class.java).apply {
                    flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK
                })
            }
            SchedulerType.NOTIFICATION.ordinal -> run {
                Timber.d("Showing 'upcoming alarm' notification")
                notificationHandler.showUpcomingNotification()
            }
            else -> Timber.e("Error while receiving alarm (no EXTRA_SCHEDULER_TYPE)")
        }

        stopSelf()
    }

    companion object {

        fun enqueueWork(context: Context, intent: Intent) {
            enqueueWork(context, AlarmJobIntentService::class.java, JOB_ID, intent)
        }
    }
}
