package com.jarsilio.android.nofussalarmclock.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.jarsilio.android.nofussalarmclock.extensions.alarmHandler
import com.jarsilio.android.nofussalarmclock.extensions.prefs
import timber.log.Timber

class AutoStart : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        if (context.prefs.isEnabled) {
            if (intent.action == Intent.ACTION_BOOT_COMPLETED) {
                Timber.d("Received ACTION_BOOT_COMPLETED. An alarm was set. Re-setting...")
                context.alarmHandler.setAlarm(context.prefs.alarmHourOfDay, context.prefs.alarmMinute)
            } else if (intent.action == Intent.ACTION_MY_PACKAGE_REPLACED) {
                Timber.d("Received ACTION_MY_PACKAGE_REPLACED. An alarm was set. Re-setting...")
                context.alarmHandler.setAlarm(context.prefs.alarmHourOfDay, context.prefs.alarmMinute)
            }
        }
    }
}