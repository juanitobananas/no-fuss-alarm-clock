package com.jarsilio.android.nofussalarmclock.utils

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.jarsilio.android.nofussalarmclock.MainActivity
import com.jarsilio.android.nofussalarmclock.R
import com.jarsilio.android.nofussalarmclock.extensions.prefs
import com.jarsilio.android.nofussalarmclock.extensions.utils
import java.util.Calendar
import android.content.BroadcastReceiver
import com.jarsilio.android.nofussalarmclock.extensions.alarmHandler
import com.jarsilio.android.nofussalarmclock.extensions.ringtoneHandler
import com.jarsilio.android.nofussalarmclock.models.SingletonHolder
import timber.log.Timber

const val NOTIFICATION_ID: Int = 100
const val CHANNEL_ID: String = "Alarm notification channel"
const val ACTION_SNOOZE: String = "ACTION_SNOOZE"
const val ACTION_DISMISS: String = "ACTION_DISMISS"

enum class NotificationType { UPCOMING, WAKIEWAKIE, SNOOZED }

class NotificationHandler private constructor(private val context: Context) {

    private val notificationManager: NotificationManager =
        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

    private val snoozePendingIntent: PendingIntent
        get() {
            val snoozeIntent = Intent(context, NotificationReceiver::class.java).apply {
                action = ACTION_SNOOZE
            }
            return PendingIntent.getBroadcast(context, 0, snoozeIntent, 0)
        }

    private val dismissPendingIntent: PendingIntent
        get() {
            val dismissIntent = Intent(context, NotificationReceiver::class.java).apply {
                action = ACTION_DISMISS
            }
            return PendingIntent.getBroadcast(context, 0, dismissIntent, 0)
        }

    private val tapPendingIntent: PendingIntent
        get() {
            val intent = Intent(context, MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            return PendingIntent.getActivity(context, 0, intent, 0)
        }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = context.getString(R.string.notification_channel_name)
            val descriptionText = context.getString(R.string.notification_channel_description)
            val importance = NotificationManager.IMPORTANCE_LOW
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            notificationManager.createNotificationChannel(channel)
        }
    }

    fun showUpcomingNotification() {
        showNotification(NotificationType.UPCOMING)
    }

    fun showSnoozeNotification() {
        showNotification(NotificationType.SNOOZED)
    }

    fun showWakieWakieNotification() {
        showNotification(NotificationType.WAKIEWAKIE)
    }

    private fun showNotification(notificationType: NotificationType) {
        createNotificationChannel()

        val notificationBuilder = NotificationCompat.Builder(context, CHANNEL_ID).apply {
            setSmallIcon(R.drawable.alarm_black_24dp)
            priority = NotificationCompat.PRIORITY_DEFAULT

            setOngoing(true)
            setShowWhen(false)

            when (notificationType) {
                NotificationType.UPCOMING -> {
                    setContentTitle(context.getString(R.string.notification_content_title_upcoming))
                    setContentText(context.utils.getTimeString(context.prefs.alarmHourOfDay, context.prefs.alarmMinute))
                    setContentIntent(tapPendingIntent)
                    addAction(
                        R.drawable.alarm_off_black_24dp,
                        context.getString(R.string.notification_button_dismiss),
                        dismissPendingIntent
                    )
                }
                NotificationType.WAKIEWAKIE -> {
                    setContentTitle(context.getString(R.string.notification_content_title_alarm))
                    setContentText(context.getString(R.string.wakie_wakie))
                    addAction(
                        R.drawable.alarm_off_black_24dp,
                        context.getString(R.string.notification_button_snooze),
                        snoozePendingIntent
                    )
                }
                NotificationType.SNOOZED -> {
                    val calendar = Calendar.getInstance()
                    calendar.add(Calendar.MINUTE, context.prefs.snoozeTime)
                    setContentTitle(context.getString(R.string.notification_content_title_upcoming))
                    setContentText(context.getString(R.string.snoozed_until, context.utils.getTimeString(calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE))))
                    setContentIntent(tapPendingIntent)
                    addAction(
                        R.drawable.alarm_off_black_24dp,
                        context.getString(R.string.notification_button_dismiss),
                        dismissPendingIntent
                    )
                }
            }
        }

        with (NotificationManagerCompat.from(context)) {
            notify(NOTIFICATION_ID, notificationBuilder.build())
        }
    }

    fun hideNotifications() {
        notificationManager.cancelAll()
    }

    companion object : SingletonHolder<NotificationHandler, Context>(::NotificationHandler)
}

class NotificationReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        if (intent != null) {
            val action = intent.action
            if (action != null) {
                if (action == ACTION_DISMISS) {
                    Timber.d("User tapped 'dismiss' button. Canceling all alarms.")
                    context.alarmHandler.cancelAlarms()
                    context.ringtoneHandler.stopRingtone()
                    context.prefs.isEnabled = false
                } else if (action == ACTION_SNOOZE) {
                    Timber.d("User tapped 'snooze' button. Canceling all alarms.")
                    context.alarmHandler.snooze()
                }
            }
        }
    }
}