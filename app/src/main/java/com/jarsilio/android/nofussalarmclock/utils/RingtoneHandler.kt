package com.jarsilio.android.nofussalarmclock.utils

import android.content.Context
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.MediaPlayer
import android.media.Ringtone
import android.media.RingtoneManager
import android.os.Build
import com.jarsilio.android.nofussalarmclock.extensions.prefs
import com.jarsilio.android.nofussalarmclock.models.SingletonHolder
import timber.log.Timber

class RingtoneHandler private constructor(private val context: Context) {

    private var ringtone: Ringtone? = null
    private var mediaPlayer: MediaPlayer? = null

    fun playRingtone() {
        ringtone = RingtoneManager.getRingtone(context, context.prefs.ringtone)
        Timber.d("Playing ringtone")
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
            mediaPlayer = MediaPlayer().apply {
                setAudioStreamType(AudioManager.STREAM_ALARM)
                setDataSource(context, context.prefs.ringtone)
                isLooping = true
                prepare()
                start()
            }
        } else {
            ringtone?.audioAttributes = AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_ALARM).build()
            ringtone?.isLooping = true
            ringtone?.play()
        }
    }

    fun stopRingtone() {
        Timber.d("Stopping ringtone")
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.P) {
            try {
                mediaPlayer?.stop()
                mediaPlayer?.release()
            } catch (e: IllegalStateException) {
                Timber.e("Failed to stop (or release) mediaPlayer: $e")
            }
        } else {
            ringtone?.stop()
        }
    }

    companion object : SingletonHolder<RingtoneHandler, Context>(::RingtoneHandler)
}