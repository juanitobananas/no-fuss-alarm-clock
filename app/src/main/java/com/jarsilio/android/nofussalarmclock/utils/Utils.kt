package com.jarsilio.android.nofussalarmclock.utils

import android.content.Context
import com.jarsilio.android.nofussalarmclock.models.SingletonHolder
import android.text.format.DateFormat
import com.jarsilio.android.nofussalarmclock.extensions.prefs
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.Calendar

class Utils private constructor(private val context: Context) {
    private val timeFormat
        get() = if (DateFormat.is24HourFormat(context)) {
            SimpleDateFormat("HH:mm")
        } else {
            SimpleDateFormat("hh:mm a")
        }

    fun getTimeString(calendar: Calendar): String {
        return timeFormat.format(calendar.time)
    }

    fun getTimeString(hourOfDay: Int, minute: Int): String {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay)
        calendar.set(Calendar.MINUTE, minute)
        return getTimeString(calendar)
    }

    fun getAlarmCalendar(hourOfDay: Int, minute: Int): Calendar {
        val alarmCalendar = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, hourOfDay)
            set(Calendar.MINUTE, minute)
            set(Calendar.SECOND, 0)
        }

        val nowTime = Calendar.getInstance().apply {
            set(Calendar.SECOND, 0)
        }

        if (alarmCalendar.timeInMillis - nowTime.timeInMillis < 0) { // Alarm is yesterday
            Timber.d("Time is before now, adding one day to alarm Calendar object")
            alarmCalendar.add(Calendar.DAY_OF_YEAR, 1)
        }

        return alarmCalendar
    }

    fun getTimeSinceAlarm(): Int {
        val alarmCalendar = Calendar.getInstance().apply {
            set(Calendar.HOUR_OF_DAY, context.prefs.alarmHourOfDay)
            set(Calendar.MINUTE, context.prefs.alarmMinute)
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }

        val nowTime = Calendar.getInstance().apply {
            set(Calendar.SECOND, 0)
            set(Calendar.MILLISECOND, 0)
        }

        if (nowTime.timeInMillis - alarmCalendar.timeInMillis < 0) { // Alarm was yesterday but snoozed until today (somehow around midnight)
            Timber.d("Alarm was yesterday. Recalculating 'timeSinceAlarm'")
            alarmCalendar.add(Calendar.DAY_OF_YEAR, -1)
        }

        val timeSinceAlarm = ((nowTime.timeInMillis - alarmCalendar.timeInMillis) / 1000 / 60).toInt()
        Timber.v("Time since alarm (in minutes): $timeSinceAlarm")

        return timeSinceAlarm
    }

    fun getTwoHoursBeforeCalendar(calendar: Calendar): Calendar {
        return (calendar.clone() as Calendar).apply {
            add(Calendar.HOUR_OF_DAY, -2)
        }
    }

    companion object : SingletonHolder<Utils, Context>(::Utils)
}