package com.jarsilio.android.nofussalarmclock.utils

import android.content.Context
import android.os.Build
import android.os.VibrationEffect
import android.os.Vibrator
import com.jarsilio.android.nofussalarmclock.extensions.prefs
import com.jarsilio.android.nofussalarmclock.models.SingletonHolder
import timber.log.Timber

class VibrationHandler private constructor(private val context: Context) {

    private var vibrationThread: Thread? = null

    private fun vibrate(pattern: LongArray) {
        val vibrator = context.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator?

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator?.vibrate(
                VibrationEffect.createWaveform(
                    pattern,
                    VibrationEffect.DEFAULT_AMPLITUDE
                )
            )
        } else {
            @Suppress("DEPRECATION")
            vibrator?.vibrate(pattern, -1)
        }
    }

    fun startVibrationIfNecessary() {
        if (!context.prefs.vibrate) {
            return
        }

        val vibratePattern = arrayOf(0L, 600L, 800L).toLongArray()

        vibrationThread = Thread(Runnable {
            Timber.d("Vibrating from thread")
            try {
                while (true) {
                    vibrate(vibratePattern)
                    Thread.sleep(vibratePattern.sum()) // Each vibrate starts in the background already, so better wait till it's done vibrating
                }
            } catch (e: InterruptedException) {
                Timber.d("Interrupted vibration thread.")
            }
        })

        vibrationThread?.start()
    }

    fun stopVibrationIfNecessary() {
        Timber.d("Stopping vibration if necessary")
        if (vibrationThread != null) {
            Timber.d("Trying to interrupt thread")
            vibrationThread!!.interrupt()
            vibrationThread = null
        }
    }

    companion object : SingletonHolder<VibrationHandler, Context>(::VibrationHandler)
}